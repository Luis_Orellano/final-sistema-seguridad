import { serviciosDeEmergencias } from "./database.service.js";
import { Servicio } from "./servicios/Servicio.class.js";
import { ServiciosDeEmergencias } from "./servicios/ServiciosDeEmergencias.class.js";
import { $ } from "./tools.service.js";

export class UI {
  constructor() {}

  //Metodo de la clase UI encargada de iniciar las card en el DOM
  changeContentCard() {
    let elem = $("#carousel");

    console.log(serviciosDeEmergencias);
    serviciosDeEmergencias.map(
      (servicio: ServiciosDeEmergencias, index: number) => {
        let html = this.cardContent(servicio, index);

        elem!.insertAdjacentHTML("beforeend", html);

        let button = $(`#check-object${index}`);

        button.onclick = () => {
          checkObjectSelected(index);
        };
      }
    );
  }

  private cardContent(servicio: ServiciosDeEmergencias, index: number): string {
    let html: string = "";
    return (html += `<div class="carousel-item ${index == 0 && "active"}">
          <div class="card text-center">
            <div class="card-header">${servicio.sensor.getName()}</div>
              <div class="card-body">
                <h5 class="card-title">Temperatura actual: 
                  <span id="sensor-state${index}">${servicio.sensor.getTemperature()}</span>
                </h5>
                  <p id="services${index}">Servicios: ${servicio.services.map(
      (servicio) => servicio.getName()
    )}</p>
                  <span id="notify${index}" class="pb-4">${servicio.notifyCall()}</span>
                    <br>
                <button type="button" class="btn btn-primary" id="check-object${index}" data-toggle="modal" data-target="#exampleModal">Agregar Servicio</button>
              </div>
            </div>
            <div class="card-footer bg-success text-white" id="change-state${index}">${stateSensor(
      servicio.sensor.getState(),
      index
    )}</div>
          </div>
        </div>`);
  }
}

//agrega y quita el fondo del footer de la card
function changeClass(index: number, bg: string) {
  let state = $(`#change-state${index}`);
  if (bg === "danger") {
    state.classList.remove("bg-success");
    state.classList.add(`bg-${bg}`);
  } else {
    if (state?.classList.contains("bg-danger")) {
      state.classList.remove("bg-danger");
    }
    state?.classList.add(`bg-${bg}`);
  }
}

//Si el estado del sensor esta en false temperatura = estable
//Si el estado del sensor esta en true se activa la alerta y se cambia el fondo de color
function stateSensor(state: boolean, index: number): string {
  console.log(state);

  if (!state) {
    changeClass(index, "success");
    return `La temperatura es estable`;
  } else {
    changeClass(index, "danger");
    return "El sensor esta en ALERTA!";
  }
}

//metodo para actualizar el DOM en un intervalo de X segundos
//tambien encargado de randomizar el valor de la temperatura actual,
//para que se notifique al observador y ejecutar el notify del observer
setInterval(async () => {
  serviciosDeEmergencias.forEach(
    (servicio: ServiciosDeEmergencias, index: number) => {
      servicio.sensor.setTemperature(
        +(Math.random() * (100 - 30) + 30).toFixed(2)
      );
      let sensorState = $(`#sensor-state${index}`);

      while (sensorState.firstChild) {
        sensorState.removeChild(sensorState.firstChild);
      }

      sensorState.insertAdjacentHTML(
        "beforeend",
        servicio.sensor.getTemperature().toString()
      );
      //fin de set temperatura random al sensor

      //set estado actual del sensor
      let changeSensor = $(`#change-state${index}`);

      while (changeSensor.firstChild) {
        changeSensor.removeChild(changeSensor.firstChild);
      }

      let state: string = stateSensor(
        servicio.sensor.checkTemperature(),
        index
      );

      changeSensor.insertAdjacentHTML("beforeend", state);
      //fin del set estado actual del sensor

      //set notify
      let notify = $(`#notify${index}`);

      while (notify.firstChild) {
        notify.removeChild(notify.firstChild);
      }

      notify.insertAdjacentHTML("beforeend", servicio.notifyCall());
    }
  );
}, 2000);

//clearInterval(interval);

//funcion para obtener los datos del formulario y crear un servicio nuevo
//el servicio nuevo se crea al Objeto correspondiente,
//para identificarlo puse un span hidden en el html el cual guarda una referencia al objeto
$("#my-form").onsubmit = function () {
  let name = (<HTMLInputElement>$("#name")).value;
  let telefono = +(<HTMLInputElement>$("#telefono")).value;

  if (name != "" && telefono != null) {
    let newService = new Servicio(name, telefono);

    let selectObj = $("#selected-object").textContent || "0";

    serviciosDeEmergencias[+selectObj].addService(newService);
  }

  $("#salir-button").click();
  return false;
};

//onclick para que actualize los nombre de los servicios en el DOM
//cuando se termina de agregar un servicio
$("#salir-button").onclick = () => {
  updateNameOfService();
};

//metodo para actualizar el DOM cuando se agrega un nuevo servicio al Sensor
function updateNameOfService() {
  let selectObj = $("#selected-object").textContent || "0";
  console.log(selectObj);

  let contentNewService = $(`#services${+selectObj}`);
  console.log(contentNewService);

  while (contentNewService.firstChild) {
    contentNewService.removeChild(contentNewService.firstChild);
  }

  contentNewService.insertAdjacentHTML(
    "beforeend",
    "Servicios: " +
      serviciosDeEmergencias[+selectObj].services.reduce(
        (anterior, service) => {
          return anterior + (anterior === "" ? "" : ", ") + service.name;
        },
        ""
      )
  );
}

//Metodo encargado de checkear cual objeto se selecciono al hacer click en el boton agregar servicio
//esta referencia se guarda en un span agregado al html con el id=selected-object
function checkObjectSelected(index: number) {
  let elem = $("#selected-object");

  while (elem.firstChild) {
    elem.removeChild(elem.firstChild);
  }

  elem.insertAdjacentHTML("beforeend", index.toString());
}
