export class Servicio {
  phone: number;
  name: string;

  constructor(name: string, phone: number) {
    this.name = name;
    this.phone = phone;
  }

  call(): string {
    return `Realizando llamada al numero ${this.phone}
    ${this.name} en camino!! <br>`;
  }

  getName(): string {
    return this.name;
  }

  getPhone(): number {
    return this.phone;
  }
}
