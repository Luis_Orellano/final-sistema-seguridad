import { Observer } from "../interface/Observer.interface.js";
import { Sensor } from "../interface/Sensor.interface.js";
import { Servicio } from "./Servicio.class.js";

export class ServiciosDeEmergencias implements Observer {
  services: Servicio[];
  sensor: Sensor;

  constructor(sensor: Sensor, services: Servicio[]) {
    this.sensor = sensor;
    this.services = services;
  }

  addService(service: Servicio): void {
    this.services.push(service);
  }

  removeService(service: Servicio): void {
    let index = this.services.indexOf(service);
    if (index !== -1) {
      this.services.splice(index, 1);
    }
  }

  notifyCall(): string {
    if (this.sensor.state) {
      return this.services.reduce(
        (callAnterior: string, callActual: Servicio) => {
          return callAnterior + callActual.call();
        },
        ""
      );
    }
    return "";
  }
}
