import { Alarma } from "./dispositivos/Alarma.class.js";
import { Rociador } from "./dispositivos/Rociador.class.js";

export const Store: any = {
  Alarma,
  Rociador,
};
