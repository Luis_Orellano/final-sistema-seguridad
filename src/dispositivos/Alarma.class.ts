import { Sensor } from "../interface/Sensor.interface.js";

export class Alarma implements Sensor {
  name: string;
  temperature: number;
  port: number;
  state: boolean;
  temperatureMax: number;

  constructor(
    name: string,
    temperature: number,
    port: number,
    temperatureMax: number,
    state: boolean
  ) {
    this.name = name;
    this.temperature = temperature;
    this.port = port;
    this.state = state;
    this.temperatureMax = temperatureMax;
  }

  checkTemperature(): boolean {
    if (!this.state) {
      if (this.temperature >= this.temperatureMax) {
        this.state = true;
        return true;
      } else {
        return false;
      }
    } else {
      this.state = false;
      return false;
    }
  }

  getTemperature(): number {
    return this.temperature;
  }

  setTemperature(temperature: number): void {
    this.temperature = temperature;
  }

  getName(): string {
    return this.name;
  }

  getState(): boolean {
    return this.state;
  }
}
