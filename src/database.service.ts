import { Alarma } from "./dispositivos/Alarma.class.js";
import { Rociador } from "./dispositivos/Rociador.class.js";
import { Servicio } from "./servicios/Servicio.class.js";
import { ServiciosDeEmergencias } from "./servicios/ServiciosDeEmergencias.class.js";
import { element, $, $$ } from "./tools.service.js";
import { Store } from "./SensorStore.js";

const API: string = "http://localhost:3000";

export async function getData() {
  const response = await window.fetch(`${API}/dispositives`);
  console.log(response);

  if (response.ok) return response.json();
  else return null;
}

export const serviciosDeEmergencias: ServiciosDeEmergencias[] = await getData()
  .then((response) => {
    return response.map(
      (dispositive: any) =>
        new ServiciosDeEmergencias(
          new Store[dispositive.name](
            dispositive.name,
            dispositive.temperature,
            dispositive.port,
            dispositive.temperatureMax,
            dispositive.state
          ),
          dispositive.servicios.map(
            (servicio: any) => new Servicio(servicio.name, servicio.telefono)
          )
        )
    );
  })
  .catch((error) => {
    console.log(error);
    return null;
  });
