export interface Sensor {
  name: string;
  temperature: number;
  port: number;
  state: boolean; //estado activo: true ; inactivo: false
  temperatureMax: number;

  checkTemperature(): boolean;
  getTemperature(): number;
  setTemperature(temperature: number): void;
  getName(): string;
  getState(): boolean;
}
