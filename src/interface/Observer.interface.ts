import { Servicio } from "../servicios/Servicio.class.js";

export interface Observer {
  addService(servicio: Servicio): void;
  removeService(servicio: Servicio): void;
  notifyCall(): void;
}
