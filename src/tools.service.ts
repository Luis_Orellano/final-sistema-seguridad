export let element = $("#descripcion");

export function $(selector: string): HTMLElement {
  return document.querySelector(selector) as HTMLElement;
}

export function $$(selector: string): NodeListOf<HTMLElement> {
  return document.querySelectorAll(selector) as NodeListOf<HTMLElement>;
}
